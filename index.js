let trainer = {};

trainer.name="Ash Ketchum";
trainer.age=10;
trainer.pokemon=["Pikachu","Charizard", "Squirtle","Bulbasaur"];
trainer.friend={
		hoenn:["May","Max"],
		kanto:["Brock", "Misty"]
};
trainer.talk=function(pokemon){
			console.log(pokemon +"! I choose you");
		};

console.log(trainer);
console.log("Result from dot notation: ");
console.log(trainer.name);
console.log("Result from the square bracket notation: ");
console.log(trainer['pokemon']);
console.log("Result from the object methods: ");
trainer.talk(trainer.pokemon[0]);

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;
	this.tackle = function(target){
		target.healthRemain=target.health-this.attack;
		target.health=target.healthRemain;
		console.log(this.name +" tackled "+ target.name);
		console.log(target.name+"'s health is now reduce to "+target.healthRemain);
		if(target.healthRemain<=0){
			target.faint();	
		}
		
	}
	this.faint = function(){
		console.log(this.name+ " fainted.");
	}
}
let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);
console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);
mewtwo.tackle(geodude);
console.log(geodude);

